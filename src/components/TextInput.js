import styled from 'styled-components/native';

const Input = styled.TextInput`
    paddingHorizontal: 20px;
    paddingVertical: 15px;
    borderRadius: 10px;
    backgroundColor: #FFF;
    alignSelf: stretch;
    marginBottom: 15px;
    marginHorizontal: 20px;
    fontSize: 16px;
    textAlign: center;
`;

export default Input;