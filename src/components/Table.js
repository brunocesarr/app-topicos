import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Alert } from 'react-native';
import { Table, TableWrapper, Row, Rows, Cell } from 'react-native-table-component';

export default class TableUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tableHead: ['Nome', 'Sobrenome', 'Ações'],
      tableData: [
        ['Bruno', 'Lápis', '3'],
        ['Bruno', 'Lápis', '3'],
        ['Lopes', 'Pablo', 'c'],
      ]
    }
  }

  _alertIndex(index) {
    Alert.alert(`Detalhes da linha ${index + 1}!`);
  }

  render() {
    const state = this.state;
    const element = (data, index) => (
      <TouchableOpacity onPress={() => this._alertIndex(index)}>
        <View style={styles.btn}>
          <Text style={styles.btnText}>Detalhes</Text>
        </View>
      </TouchableOpacity>
    );
 
    return (
      <View style={styles.container}>
        <Table borderStyle={{borderColor: 'transparent'}}>
          <Row data={state.tableHead} style={styles.head} textStyle={styles.text}/>
          {
            state.tableData.map((rowData, index) => (
              <TableWrapper key={index} style={styles.row}>
                {
                  rowData.map((cellData, cellIndex) => (
                    <Cell key={cellIndex} data={cellIndex === 2 ? element(cellData, index) : cellData} textStyle={styles.text}/>
                  ))
                }
              </TableWrapper>
            ))
          }
        </Table>
      </View>
    )
  }
}

const styles = StyleSheet.create({
    container: {
        padding: 16, 
        paddingTop: 30, 
        backgroundColor: '#fff' 
    },
    head: { 
        height: 40,
        backgroundColor: '#869146',
    },
    text: { 
        margin: 6,
        color: 'white' 
    },
    row: { 
        flexDirection: 'row', 
        backgroundColor: '#a7a0d1' 
    },
    btn: { 
        width: 100, 
        height: 30, 
        backgroundColor: '#594d9f',
        padding: 5,
        margin: 10,  
        borderRadius: 50 
    },
    btnText: { 
        textAlign: 'center', 
        color: '#fff' 
    }
});
