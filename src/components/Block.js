import styled from 'styled-components';

const Block = styled.View`
    width: 90%;
    height: 150px;
    backgroundColor: transparent
    marginBottom: 15px;
    marginTop: 30px;
    marginHorizontal: 20px;
    borderRadius: 5px;
    alignItems: center;
`;

export default Block;