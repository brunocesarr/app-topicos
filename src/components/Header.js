import React, { PureComponent } from "react";
import { StyleSheet, Text, View, Image } from 'react-native';

import Icon from '../assets/icon.png';

export default class Header extends PureComponent {
  render() {
    return (
      <View style={styles.componentHeader}>
        <Image
          source={Icon}
          style={styles.logo}
        />
        <Text style={styles.textHeader}>App Tópicos</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    componentHeader: {
        padding: 15,
        position: 'relative',
        backgroundColor: '#7265c2',
        flexDirection:  'row',
        justifyContent:'space-between'
    },
    textHeader: {
      fontWeight: 'normal',
      fontSize: 20,
      marginTop: 5,
      textAlign: 'center',
      color: '#fff',
    },
    logo: {
      width: 40,
      height: 40,
    }  
});