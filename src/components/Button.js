import styled from 'styled-components';

const Button = styled.TouchableHighlight`
    background: ${props => props.primary ? "green" : "black"};
    padding: 20px;
    borderRadius: 5px;
    alignSelf: stretch;
    margin: 15px;
    marginHorizontal: 20px;
`;

 export default Button;