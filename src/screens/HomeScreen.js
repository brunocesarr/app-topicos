import * as React from 'react';
import { 
  StyleSheet,
  Text,
  View
} from 'react-native';

import Header from "../components/Header";
import Table from "../components/Table";
import Block from "../components/Block";

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    title: <Text>'Home'</Text>
  };

  render() {
    return (
      <View>
        <Header/>
        <Table/>
        <Block>
          <Text style={styles.text}>
            Seja Bem Vindo ao App do Sucesso!
          </Text>
        </Block>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  logo: {
    width: 200,
    height: 200,
    borderRadius: 20,
    marginBottom: 20
  },
  text: {
    color: '#000',
  }
});