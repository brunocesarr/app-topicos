import * as React from 'react';

import { 
  Text, 
  StyleSheet, 
  View, 
  Image, 
  Alert
 } from 'react-native';

 import { onSignIn } from "../Auth";

import Icon from '../assets/icon.png';
import Input from '../components/TextInput';
import Button from '../components/Button';

export default ({ navigation }) => (  
  <View style={styles.container}>
    <Image
      source={Icon}
      style={styles.logo}
    />
    <Input
      placeholder="Endereço de e-mail"
      autoCapitalize="none"
      autoCorrect={false}
    />
    <Input
      placeholder="Senha"
      autoCapitalize="none"
      autoCorrect={false}
      secureTextEntry
    />
        
    <Button onPress={() => {
      onSignIn().then(() => navigation.navigate("SignedIn"));
    }} primary>
      <Text style={styles.botaoText}>Login</Text>      
    </Button>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#121212',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 200,
    height: 200,
    borderRadius: 20,
    marginBottom: 20
  },
  botaoText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#fff',
    textAlign: 'center',
  }
})