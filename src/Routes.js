import React from "react";
import { 
    Platform, 
    StatusBar,
    StyleSheet
} from "react-native";
import {
  StackNavigator,
  TabNavigator,
  SwitchNavigator,
  createSwitchNavigator,
  createAppContainer
} from "react-navigation";
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';
import { FontAwesome } from "react-native-vector-icons";

import HomeScreen from './screens/HomeScreen';
import LoginScreen from './screens/LoginScreen';
import RegisterScreen from './screens/RegisterScreen';
import ProfileScreen from './screens/ProfileScreen';

const headerStyle = {
    marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
};
  

export const SignedOut = createStackNavigator({
    SignUp: {
      screen: LoginScreen,
      navigationOptions: {
        title: "Sign In",
        headerShown: false,
        headerStyle
      }
    },
    SignIn: {
      screen: RegisterScreen,
      navigationOptions: {
        title: "Sign Up",
        headerStyle
      }
    }
  });
  
  export const SignedIn = createBottomTabNavigator(
    {
      Home: {
        screen: HomeScreen,
        navigationOptions: {
          tabBarLabel: "Home",
          tabBarIcon: ({ tintColor }) => (
            <FontAwesome name="home" size={30} color={tintColor} style={styles.logo}/>
          )
        }
      },
      Profile: {
        screen: ProfileScreen,
        navigationOptions: {
          tabBarLabel: "Profile", 
          tabBarIcon: ({ tintColor }) => (
            <FontAwesome name="user" size={30} color={tintColor} style={styles.logo}/>
          )
        }
      }
    },
    {
      tabBarOptions: {
        style: {
          paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0
        }
      }
    }
  );
  
  export const createRootNavigator = (signedIn = false) => {
    return createSwitchNavigator(
      {
        SignedIn: {
          screen: SignedIn
        },
        SignedOut: {
          screen: SignedOut
        }
      },
      {
        initialRouteName: signedIn ? "SignedIn" : "SignedOut"
      }
    );
};


const styles = StyleSheet.create({
    logo: {
      marginBottom: 20,
    },
});
  