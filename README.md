# Projeto App Tópicos

Este projeto visa realização de uma aplicação final desenvolvida durante a disciplina de Tópicos II.

## Requisitos

<ol>
    <li>Node.js e NPM</li>
    <li>Expo</li>
    <li>Docker e Docker-Compose</li>
</ol>

