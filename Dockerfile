FROM ubuntu:16.04

ENV DEBIAN_FRONTEND=noninteractive

# Update packages
RUN apt-get update -y && apt-get upgrade -y

# Install some packages we need
RUN apt-get install -y build-essential git curl python jq

# Install latest version of pip
RUN curl -O https://bootstrap.pypa.io/get-pip.py && python get-pip.py

# Install Node.JS
RUN cd /usr/local && curl http://nodejs.org/dist/v13.1.0/node-v13.1.0-linux-x64.tar.gz | tar --strip-components=1 -zxf- && cd
RUN npm -g update npm

# Install AWS CLI
RUN pip install awscli awsebcli
RUN npm install --silent
RUN npm install react-scripts@3.0.1 -g --silent
#RUN npm install expo-cli@3.5.0 --global
RUN npm install expo-cli --global

WORKDIR /app

COPY package.json .
COPY . .

EXPOSE 19000
EXPOSE 19001
EXPOSE 19002
EXPOSE 19003

# Make sure we land in a shell
CMD ["/bin/bash"]
CMD ["npm", "start"]